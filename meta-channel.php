<meta charset="utf-8">
<title><?php echo $title ?> - Watch Live TV Online Channels, Sports, Streams For Free - <?php echo $site_name; ?></title>
<meta property="og:title" content="<?php echo $title ?>">
<meta property="og:type" content="video.tv_show">
<meta property="og:url" content="<?php echo curPageURL(); ?>">
<meta property="og:image" content="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/images/<?php echo $file ?>.jpg">
<meta property="og:site_name" content="<?php echo $site_name; ?>">
<meta property="og:description" content="Watch <?php echo $title ?> Live TV Online For Free - <?php echo $site_name; ?> - Watch Live TV Online Channels, Sports, Streams For Free">
<meta name="description" content="Watch <?php echo $title ?> Live TV Online For Free - <?php echo $site_name; ?> - Watch Live TV Online Channels, Sports, Streams For Free">
<link rel="alternate" href="http://feeds.feedburner.com/LiveOnlineTv247" title="<?php echo $site_name; ?>" type="application/rss+xml">
<link rel="canonical" href="<?php echo curPageURL(); ?>">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
<?php include("analyticstracking.php") ?>
<link href="/modern.css" rel="stylesheet">



<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-522cce361b53a0ca"></script>
<script type="text/javascript">addthis.layers({'theme':'transparent','share':{'position':'left','numPreferredServices':5},'follow':{'services':[{'service':'facebook','id':'Onlinetvmania'}
,{'service':'twitter','id':'OnlineTVManiaco'},{'service':'google_follow','id':'105710286084493435302'}

]},'whatsnext':{}});</script>
<!-- AddThiNNER&tagid=9" ></script>
