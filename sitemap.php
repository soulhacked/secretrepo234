<?php header('Content-Type: application/xml');
echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/livesports.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/livecricket.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/livefootball.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/schedule.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream1.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream2.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream3.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream4.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream5.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream6.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream7.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream8.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream9.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream10.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream11.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream12.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream13.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream14.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream15.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream16.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream17.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream18.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream19.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream20.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream21.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream22.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream23.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream24.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stream25.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/starcricket.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/eurosport.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/eurosport2.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/wwetv.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/skysports1.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/skysports2.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/skysports3.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/skysports4.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/skysportsnews.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/skysportsf1.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/skysportscricket.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/espnuk.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/espn.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/espn2.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/starsports.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/setantasports.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/setantasportsasia.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/setantasportsaustralia.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/chelseatv.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/nflnetwork.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/nbatv.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/nhlnetwork.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/golfchannel.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/tencricket.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/tensports.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/geosuper.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/ptvsports.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/skypoker.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/bbcone.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/bbctwo.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/itv1.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/itv2.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/abc.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/amc.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/cbs.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/hbo.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/hbohd.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/hboplus.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/usanetwork.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/cwtv.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/fox.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/fx.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/foxmovies.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/abcfamily.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/starmovies.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/syfy.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/tnt.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/channel5.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/bbc.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/foxnews.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/cnn.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/nbc.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/msnbc.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/skynews.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/bloomberg.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/cnbctv18.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/cnbctv18primehd.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/timesnow.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/headlinestoday.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/newsx.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/mtv.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/mtuneshd.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/9xm.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/vh1.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/channelv.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/e24.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/history.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/natgeo.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/animalplanet.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/discoveryhd.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/discovery.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/cartoonnetwork.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/disneychannel.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/nick.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/pogo.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/disneyjunior.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/disneyxd.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/starplus.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/starplushd.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/lifeok.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/colors.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/sonytv.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/sabtv.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/zeetv.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/setmax.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/stargold.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/zeecinema.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/saharaone.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/utvmovies.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/ptcnews.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/ptcpunjabi.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/abpnews.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/aajtak.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/delhiaajtak.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/tez.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/cnbcawaaz.php</loc>
  <changefreq>always</changefreq>
</url>
<url>
  <loc>http://<?php echo $_SERVER["HTTP_HOST"]; ?>/indiatv.php</loc>
  <changefreq>always</changefreq>
</url>
</urlset>