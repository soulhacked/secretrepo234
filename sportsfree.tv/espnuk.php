<?php include("phpheader.php") ?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php 
$title = "ESPN UK"; 
?>
<?php include("meta-channel.php") ?>
</head>
<body>
<?php include("header-channel.php") ?>
<p><iframe frameborder="0" marginheight="0" marginwidth="0" src="/embed/<?php echo $file ?>.php" name="myfr" scrolling="no" width="650" height="550"></iframe></p>
<p>
<a href="espn.php" target="_blank" class="button big bg-color-darken fg-color-white">ESPN</a>
<a href="espn2.php" target="_blank" class="button big bg-color-darken fg-color-white">ESPN 2</a>
<a href="espnuk.php" target="_blank" class="button big bg-color-darken fg-color-white">ESPN UK</a>
</p>
<?php include("footer-channel.php") ?>
</body>
</html>