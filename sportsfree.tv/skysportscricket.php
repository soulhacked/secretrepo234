<?php include("phpheader.php") ?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php 
$title = "Sky Sports Cricket"; 
?>
<?php include("meta-channel.php") ?>
</head>
<body>
<?php include("header-channel.php") ?>
<p><iframe frameborder="0" marginheight="0" marginwidth="0" src="/embed/<?php echo $file ?>.php" name="myfr" scrolling="no" width="650" height="550"></iframe></p>
<p><a href="https://play.google.com/store/apps/developer?id=Greenlie.org" target="_blank" class="button big bg-color-green fg-color-white icon-download">Live Cricket TV (2G/3G) Android App!</a></p>
<?php include("footer-channel.php") ?>
</body>
</html>