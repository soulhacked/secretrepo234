<?php
if ($_SERVER["HTTP_HOST"] == 'www.usliveonlinetv.com') { $site_name='US Live Online TV'; }
else { $site_name='SportsFree.TV'; }
?>
<?php
function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}
?>
<?php
$path = curPageURL();
$file = basename($path, ".php");
?>